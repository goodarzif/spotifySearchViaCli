import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import 'rxjs/Rx';

@Injectable()
export class SpotifyService {
  static BASE_URL: string = 'https://api.spotify.com/v1';
  constructor(private http: Http) {
  }
  query(url: string, params?: Array<string>): Observable<any[]> {
    let queryUrl = `${SpotifyService.BASE_URL}${url}`;
    if(params) {
       queryUrl = `${queryUrl}?${params.join('&')}`;
    }
    return this.http.get(queryUrl).map((res:Response)=>{
      return res.json();});
  }
  search(query: string,type: string): Observable<any[]> {
    return this.query('/search/',[`q=${query}`,`type=${type}`]);
  }
  getTrack(id: string):Observable<any[]> {
    return this.query(`/tracks/${id}`);
  }
  getArtist(id: string):Observable<any[]> {
    return this.query(`/artists/${id}`);
  }
  getAlbum(id: string): Observable<any[]> {
    return this.query(`/albums/${id}`);
  }
}
