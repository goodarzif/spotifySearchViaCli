import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {SpotifyService} from "../spotify.service";
@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit {
  track: Object;
  id: string;
  constructor(private route: ActivatedRoute,private location:Location,private spotify: SpotifyService) {
    route.params.subscribe(params => {this.id = params['id'];
    });
  }

  ngOnInit() {
    this.spotify.getTrack(this.id).subscribe((res: any) => {
      this.renderTrack(res);
    });
  }
  back(): void {
    this.location.back();
  }
  renderTrack(res: any): void {
    this.track = res;
    console.log(res);
  }

}
