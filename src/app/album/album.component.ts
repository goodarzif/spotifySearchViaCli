import { Component, OnInit } from '@angular/core';
import {SpotifyService} from "../spotify.service";
import {Location} from "@angular/common";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  album: Object;
  id: string;
  constructor(private spotify: SpotifyService,private location: Location,private route: ActivatedRoute) {
    route.params.subscribe((params)=>this.id = params['id']);
  }

  ngOnInit(): void {
    this.spotify.getAlbum(this.id).subscribe((res: any)=>this.renderAlbum(res));
  }
  renderAlbum(res: any): void {
    this.album = res;
  }
  back(): void{
    this.location.back();
  }


}
