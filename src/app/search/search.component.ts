import { Component, OnInit } from '@angular/core';
import {SpotifyService} from "../spotify.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Response} from "@angular/http";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  query: string;
  results: Object;
  constructor(private spotify: SpotifyService,private route: ActivatedRoute,private router: Router) {
    this.route.queryParams.subscribe((params) => this.query = params['query'] || '');
}

  ngOnInit() {
    this.search();
  }
  search(): void {
    if(!this.query) {
      return ;
    }
    this.spotify.search(this.query,'track').subscribe((res: any) => this.renderResults(res));
  }
  submit(query: string){
    this.router.navigate(['search'], { queryParams: { query: query } }).then(_ => this.search() );
  }
  renderResults(res: any): void {
    this.results = null;
    if(res && res.tracks && res.tracks.items) {
      this.results = res.tracks.items;
    }
  }


}
