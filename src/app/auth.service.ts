import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() {}
  login(name: string,pass: string): boolean {
    if(name == 'foo' && pass == "far"){
      localStorage.setItem('username',name);
      return true;
    }else{
      return false;
    }
  }
  logout(): void {
    localStorage.removeItem('username');
  }
  isLoggedIn(): boolean{
    if(localStorage.getItem('username')) {
      return true;
    }
    return false;
  }
  getUser(): string {
    return localStorage.getItem('username');
  }

}
