import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    message: string;
  constructor(private authService: AuthService) {
    this.message = '';
  }
  ngOnInit() {
  }
  login(username: string,password: string): boolean {
    if(!this.authService.login(username,password)) {
      this.message = "the user and pass is incorrect!";
      setTimeout(function(){
        this.message = '';
      }.bind(this),3000);
      return false;
    }else {
      this.message = "you logged in successfully";
      setTimeout(function(){
        this.message = '';
      }.bind(this),3000);
      return true;
    }
  }
  logout():boolean {
    this.authService.logout();
    return false;
  }
  isAuth():boolean {
    return this.authService.isLoggedIn();
  }
}
