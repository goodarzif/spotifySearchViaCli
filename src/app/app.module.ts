import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {SpotifyService} from "./spotify.service";
import { SearchComponent } from './search/search.component';
import { TrackComponent } from './track/track.component';
import { AlbumComponent } from './album/album.component';
import { ArtistComponent } from './artist/artist.component';
import {APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from "@angular/common";
import {RouterModule, Routes} from "@angular/router";
import { LoginComponent } from './login/login.component';
import {LoggedInGuard} from "./logged-in.guard";
import {AuthService} from "./auth.service";

const routes: Routes = [
  {path:'',redirectTo:'search',pathMatch:'full'},
  {path:'Login',component:LoginComponent},
  {path:'search',component: SearchComponent},
  {path:'tracks/:id',component: TrackComponent,canActivate:[LoggedInGuard]},
  {path:'artists/:id',component: ArtistComponent,canActivate:[LoggedInGuard]},
  {path:'albums/:id',component: AlbumComponent,canActivate:[LoggedInGuard]}
];


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    TrackComponent,
    AlbumComponent,
    ArtistComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
  ],
  providers: [ SpotifyService,LoggedInGuard,AuthService,
    {provide: APP_BASE_HREF,useValue:'/'},
    {provide: LocationStrategy,useClass:HashLocationStrategy},

    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
