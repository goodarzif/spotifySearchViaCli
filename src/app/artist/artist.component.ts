import { Component, OnInit } from '@angular/core';
import {SpotifyService} from "../spotify.service";
import {Location} from "@angular/common";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {
  id: string;
  artist: Object;
  constructor(private spotify: SpotifyService,private location:Location,private route: ActivatedRoute) {
    this.route.params.subscribe((params) => this.id = params['id']);
  }
  ngOnInit() {
    this.spotify.getArtist(this.id).subscribe((res:any)=>this.renderArtist(res));
  }
  renderArtist(res: any): void {
    this.artist = res;
  }
  back(): void {
    this.location.back();
  }

}
